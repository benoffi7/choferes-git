package com.makerit.posicionmoviles.soap;

// NO OLVIDAR CAMBIAR EL PUERTO 500xx

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.makerit.posicionmoviles.objetos.Movil;

import android.content.Context;


public class conexionServicioWebSOAP  {
	
	Context contexto;
	
	public conexionServicioWebSOAP(Context _contexto)
	{
		contexto = _contexto;
	}
	

	
	// ------------------------------------------
	// ------------------------------------------
	public String ValidarUsuarioContrasena(String _usuario, String _contrasena) {

		String _rta = "0";
		
		final String NAMESPACE = "Maker";
		final String URL="http://www.makerservicios.com.ar:50099/wsAVL_Android.asmx?op=VerificarUsuarioContrasena";
		final String METHOD_NAME = "VerificarUsuarioContrasena";
		final String SOAP_ACTION = "Maker/VerificarUsuarioContrasena";
 
		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);
		
		soapObject.addProperty("usuario", _usuario);
		soapObject.addProperty("contrasena", _contrasena);
		soapObject.addProperty("control", "1");

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);
				
		// Capa de trasnsporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL,5000);
		
		try
		{
			transporte.call(SOAP_ACTION, envelope);

			SoapPrimitive response = (SoapPrimitive)envelope.getResponse();

	        _rta= String.valueOf(response.toString());
			
		}
		catch (Exception e)
		{
		//	Log.e("valor","confirmar-pedido " + e.getMessage());
			_rta = "0";
		}
		
		return _rta;
		
	}


	// ------------------------------------------
	// Obtener datos de movil
	// ------------------------------------------
	public Movil ObtenerDatosDeMovil(String _idMovil)
	{
		Movil _rta = new Movil();
		
		try
		{
			_rta = ObtenerDatosDeMovil_internet(_idMovil);
		}
		catch (Exception ex)
		{
			_rta.setLatitud(0);
		}
		
		return _rta;
	
	} 
	

	// ------------------------------------------
	// Obtener de internet la clave
	// ------------------------------------------	 
	private Movil ObtenerDatosDeMovil_internet(String _idMovil) {
		
		Movil _rta = new Movil();

		final String NAMESPACE = "Maker";
		final String URL="http://www.makerservicios.com.ar:50099/wsAVL_Android.asmx?op=ObtenerDatosMovil";
		final String METHOD_NAME = "ObtenerDatosMovil";
		final String SOAP_ACTION = "Maker/ObtenerDatosMovil";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);
		
		soapObject.addProperty("idMovil", _idMovil);

		
 
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);
				
		// Capa de trasnsporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL,10000);
	
		try
		{
			transporte.call(SOAP_ACTION, envelope);

			// Para tipos primitivos (int, string)
			//SoapPrimitive resultado =(SoapPrimitive)envelope.getResponse();

			// Para objectos
			SoapObject resultado = (SoapObject) envelope.getResponse();
						
		//	Log.e("valor","cantidad de propiedades: " + resultado.getPropertyCount());

			
			// Control de version
//			_rta.setVersionIncorrecta(Boolean.parseBoolean(resultado.getPropertyAsString("_versionIncorrecta")));
//			_rta.setActualizacionObligatoria(Boolean.parseBoolean(resultado.getPropertyAsString("_actualizacionObligatoria")));
//			_rta.setURI(resultado.getPropertyAsString("_uri"));
//			
		
			// resto
			_rta.setLatitud(Float.parseFloat(resultado.getPropertyAsString("_latitud").replace(",",".")));
			_rta.setLongitud(Float.parseFloat(resultado.getPropertyAsString("_longitud").replace(",",".")));
			_rta.setUbicacion(resultado.getPropertyAsString("_ubicacion"));
			_rta.setVelocidad(resultado.getPropertyAsString("_velocidad"));
			_rta.setUltEncuesta(resultado.getPropertyAsString("_fechaHora"));
			_rta.setTiempoDetenido(resultado.getPropertyAsString("_tiempoDetenido"));
			_rta.setDescripcion(resultado.getPropertyAsString("_descripcion"));
			
		}
		catch (Exception e)
		{
			_rta.setLatitud(0);
			return _rta;
		

		}
				
		return _rta;
		
	}


	// ------------------------------------------
	// Obtener moviles del usuario
	// ------------------------------------------	
	public Movil[] ObtenerMoviles(String _idEmpresa) {
		
		Movil[] _rta;
		Movil _movilTmp;
		 
		final String NAMESPACE = "Maker";
		final String URL="http://www.makerservicios.com.ar:50099/wsAVL_Android.asmx?op=ObtenerMovilesXEmpresa";
		final String METHOD_NAME = "ObtenerMovilesXEmpresa";
		final String SOAP_ACTION = "Maker/ObtenerMovilesXEmpresa";

		// Crear el objeto JABON (SOAP)
		SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);
		
		soapObject.addProperty("idEmpresa", _idEmpresa);


 
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(soapObject);
				
		// Capa de trasnsporte del HTTP con un timeout de 2500 milisegundos
		HttpTransportSE transporte = new HttpTransportSE(URL,10000);
	
		try
		{
			transporte.call(SOAP_ACTION, envelope);


			// Para objectos
			SoapObject resultado = (SoapObject) envelope.getResponse();
						
		 
			_rta = new Movil[resultado.getPropertyCount()];
		 
	        for (int i = 0; i < _rta.length; i++)
	        {
	               SoapObject resultadoParcial = (SoapObject)resultado.getProperty(i);
	 
	               Movil mov = new Movil();

	               mov.setDescripcion(resultadoParcial.getProperty("_descripcion").toString());
	               mov.setId(resultadoParcial.getProperty("_id").toString());
	               mov.setDominio(resultadoParcial.getProperty("_patente").toString());
	               mov.setUltEncuesta(resultadoParcial.getProperty("_fechaHora").toString());
	               mov.setUbicacion(resultadoParcial.getProperty("_ubicacion").toString());
	               mov.setVelocidad(resultadoParcial.getProperty("_velocidad").toString());
	               mov.setTransmitiendo(resultadoParcial.getProperty("_transmitiendo").toString().trim());
	               mov.setNroMovil(resultadoParcial.getProperty("_nro_movil").toString());
	               _rta[i] = mov;
	           }

			
		}
		catch (Exception e)
		{
			return null;
		

		}
				
		return _rta;
		
	}

		
}
