package com.makerit.posicionmoviles.objetos;


public class Movil {

		private float latitud;
		private float longitud;
		private String ubicacion;
		private String velocidad;
		private String tiempoDetenido;
		private String ultEncuesta;
		private String id;
		private String descripcion;
		private String dominio;
		private String transmitiendo;
		private String nroMovil;
		
		public String getNroMovil() {
			return nroMovil;
		}
		public void setNroMovil(String nroMovil) {
			this.nroMovil = nroMovil;
		}
		public String getTransmitiendo() {
			return transmitiendo;
		}
		public void setTransmitiendo(String transmitiendo) {
			this.transmitiendo = transmitiendo;
		}
		public String getDominio() {
			return dominio;
		}
		public void setDominio(String dominio) {
			this.dominio = dominio;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getDescripcion() {
			return descripcion;
		}
		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}
		public String getUltEncuesta() {
			return ultEncuesta;
		}
		public void setUltEncuesta(String ultEncuesta) {
			this.ultEncuesta = ultEncuesta;
		}
		public String getVelocidad() {
			return velocidad;
		}
		public void setVelocidad(String velocidad) {
			this.velocidad = velocidad;
		}
		public String getTiempoDetenido() {
			return tiempoDetenido;
		}
		public void setTiempoDetenido(String tiempoDetenido) {
			this.tiempoDetenido = tiempoDetenido;
		}
		public float getLatitud() {
			return latitud;
		}
		public void setLatitud(float latitud) {
			this.latitud = latitud;
		}
		public float getLongitud() {
			return longitud;
		}
		public void setLongitud(float longitud) {
			this.longitud = longitud;
		}
		public String getUbicacion() {
			return ubicacion;
		}
		public void setUbicacion(String ubicacion) {
			this.ubicacion = ubicacion;
		}
		 @Override public String toString()
		 {
			 return this.descripcion;

		 }
}
