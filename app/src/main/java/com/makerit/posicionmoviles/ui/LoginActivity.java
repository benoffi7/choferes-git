package com.makerit.posicionmoviles.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.makerit.posicionmoviles.R;
import com.makerit.posicionmoviles.funciones.Funciones;
import com.makerit.posicionmoviles.soap.conexionServicioWebSOAP;


public class LoginActivity extends AppCompatActivity {

	EditText txtUsuario, txtPass; 
	Button btnOk;
	CheckBox chkContrasena;
 	SharedPreferences preferencias;
 	String _pref_usuario, _pref_clave, _pref_idEmpresa;
 	
 	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        
        findviews();
        
        createEvents();
        
        validaPreferencias();


    }

    private boolean validaPreferencias() {

		try
		{
			preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);
			_pref_usuario = preferencias.getString("usuario", "");
			_pref_clave = preferencias.getString("clave", "");
			_pref_idEmpresa = preferencias.getString("idEmpresa", "");
					
		
			if (_pref_clave.length() > 0)
			{
				txtUsuario.setText(_pref_usuario);
				txtPass.setText(_pref_clave);
				chkContrasena.setChecked(true);

//				Intent intento = new Intent(getApplicationContext(), ListaActivity.class);
//				intento.putExtra("idEmpresa", _pref_idEmpresa);
//		
//				startActivity(intento);
//				finish();
				
			}
		}
		catch (Exception ex)
		{
			return false;
		}
		return false;
		
	}
    
    
    // ------------------------------------------
    // ------------------------------------------
    private boolean AgregarPreferencias(String _idEmpresa) {
		Boolean _rta = false;
		
		try
		{
			preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);

			// crear el editor de preferencias
			SharedPreferences.Editor editor = preferencias.edit();
			 
			// Agregar como preferencia el telefono
			editor.putString("usuario", txtUsuario.getText().toString());
			editor.putString("clave", txtPass.getText().toString());
			editor.putString("idEmpresa", _idEmpresa);
			
			editor.commit();
			
			Intent intento = new Intent(getApplicationContext(), ListaActivity.class);
			intento.putExtra("idEmpresa", _idEmpresa);
	
			startActivity(intento);
			finish();

		}
		catch (Exception ex)
		{
			_rta = false;
		}
		
		
		return _rta;
		
	}
    
	private void LimpiarPreferencias(String _idEmpresa) {
		try
		{
			preferencias = getSharedPreferences("Preferencias", Context.MODE_PRIVATE);

			// crear el editor de preferencias
			SharedPreferences.Editor editor = preferencias.edit();
			 
			// Agregar como preferencia el telefono
			editor.putString("usuario", "");
			editor.putString("clave", "");
			editor.putString("idEmpresa", "");
			
			editor.commit();
			
			//Intent intento = new Intent(getApplicationContext(), MovilActivity.class);
			Intent intento = new Intent(getApplicationContext(), ListaActivity.class);
			intento.putExtra("idEmpresa", _idEmpresa);
	
			startActivity(intento);
			finish();
		}
		catch (Exception ex)
		{

		}

	}
	
	private void findviews() {
		
		btnOk = (Button)findViewById(R.id.button_main_Ok);
		txtUsuario = (EditText)findViewById(R.id.editText_main_Usuario);
		txtPass = (EditText)findViewById(R.id.editText_main_Pass);
//		txtUsuario.setText("admintoledo");
//		txtPass.setText("admin");
		chkContrasena = (CheckBox)findViewById(R.id.checkBox_contrasena);
		
	}

	
    private void createEvents() {
    	btnOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (DatosVacios())
				{
					Toast tostada = Toast.makeText(LoginActivity.this, getResources().getString(R.string._toast_datosVacios), Toast.LENGTH_SHORT);
					tostada.setGravity(Gravity.TOP, 0, 20);
					tostada.show();
					return;
				}
				
				if (Funciones.hayInternet(LoginActivity.this))
				{
					 obtenerResultado or = new obtenerResultado();
				     or.execute(txtUsuario.getText().toString().trim(), txtPass.getText().toString().trim());
				}
				else
                {
					Builder builder = new AlertDialog.Builder (LoginActivity.this);
			        	builder.setTitle (getResources().getString(R.string.app_name))
				               .setMessage (getResources().getString(R.string._dlg_sinInternet))
				               .setNeutralButton(getResources().getString(R.string._ok),null);
              
			        builder.create().show ();
			        
		        	return;
				}
			}

			
			private boolean DatosVacios() {
				boolean respuesta = false;
				if (txtUsuario.getText().length() < 1 || txtPass.getText().length() < 1)
					respuesta = true;
				return respuesta;
			}
		});
		
	}

    
	public void Habilitar_ingreso(String _idEmpresa) {
		if (chkContrasena.isChecked() && _pref_idEmpresa.equals(""))
		{
			GuardarContrasena(_idEmpresa);
		}
		else
		{
			if (!chkContrasena.isChecked() && !_pref_idEmpresa.equals(""))
			{
				LimpiarPreferencias(_idEmpresa);
			}
			else
			{
				//Intent intento = new Intent(getApplicationContext(), MovilActivity.class);
				Intent intento = new Intent(getApplicationContext(), ListaActivity.class);
				intento.putExtra("idEmpresa", _idEmpresa);
		
				startActivity(intento);
				
				finish();
			}
		}
	}
	


	private boolean GuardarContrasena(final String _idEmp) {

        Builder builder = new AlertDialog.Builder (LoginActivity.this);
        builder.setTitle (getString(R.string.app_name))
               .setMessage (getString(R.string._dlg_guardaPass))
               .setNegativeButton (getString(R.string._no),null)
               .setPositiveButton(R.string._yes, new DialogInterface.OnClickListener() {
            	   	public void onClick(DialogInterface dialog, int id) {
            	   		AgregarPreferencias(_idEmp);
            	   	}
               });
               
        builder.create().show ();
		return false;
	}
	
	
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
        	Intent intento = new Intent(LoginActivity.this, AcercaDe.class);
        	startActivity(intento);
        	
        	
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    

	
    
    // ------------------------------------------
    // ------------------------------------------
    public class obtenerResultado extends AsyncTask<String , Void, String>
    
	{
        String user;
    	ProgressDialog pd = new ProgressDialog(LoginActivity.this);
        

        // Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getResources().getString(R.string._dlg_validandoUsuario));
			pd.show();
			pd.setCancelable(false);
			
			super.onPreExecute();
		}

		
		
		@Override
		protected String doInBackground(String... _texto) {

//			conexionWeb cw = new conexionWeb();
//			return cw.Obtener_Resultado();
			
			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(LoginActivity.this);
			
			
			String _rta;
			_rta = cw.ValidarUsuarioContrasena("" + _texto[0], "" + _texto[1]);
			
			
			return _rta;
		}
	
		@Override
		protected void onPostExecute(String resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();


			if ( !resultado.equals("0") )  
			{
				Habilitar_ingreso(resultado);
			}
			else
			{

		        Builder builder = new AlertDialog.Builder (LoginActivity.this);
		        builder.setTitle (getString(R.string.app_name))
		               .setMessage (getString(R.string._dlg_UsuarioNoValido))
		               .setNeutralButton(getString(R.string._ok),null);
	               
		        builder.create().show ();
				return;
			}
			
			super.onPostExecute(resultado);
		}

	  }


}
