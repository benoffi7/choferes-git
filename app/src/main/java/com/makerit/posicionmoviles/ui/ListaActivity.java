package com.makerit.posicionmoviles.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.makerit.posicionmoviles.R;
import com.makerit.posicionmoviles.adap.AdaptadorMoviles;
import com.makerit.posicionmoviles.objetos.Movil;
import com.makerit.posicionmoviles.soap.conexionServicioWebSOAP;

import java.util.ArrayList;

public class ListaActivity extends AppCompatActivity {

	
	// ------------------------------------------
	// Variables de componentes
	// ------------------------------------------
	ListView listView;
	Button button_Refrescar;
	AdaptadorMoviles adaptador;
	String _idEmpresa;
	ArrayList<Movil> listadoMoviles;

	
	// ------------------------------------------
	// Constructor
	// ------------------------------------------
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lista);
		
	  	if (getIntent() != null)
		{
			// Tiene extras ? 
			if (getIntent().getExtras() != null)
			{
				if (getIntent().getExtras().getString("idEmpresa") != null)
				{
					_idEmpresa = getIntent().getExtras().getString("idEmpresa");
				}
			}
		}

	  	button_Refrescar = (Button)findViewById(R.id.button_refrescar);
	  	listView = (ListView)findViewById(R.id.listView1);	  	
	  	listView.setVisibility(View.INVISIBLE);
	  	
		obtenerMoviles om = new obtenerMoviles();
		om.execute(_idEmpresa);

		setEvents();
		
	}
	
	@Override
	public boolean onSupportNavigateUp(){
	    finish();
	    return true;
	}
	private void setEvents() {

        findViewById(R.id.imageView2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(ListaActivity.this,AcercaDe.class));
            }
        });
		button_Refrescar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				listView.setVisibility(View.INVISIBLE);
				obtenerMoviles om = new obtenerMoviles();
				om.execute(_idEmpresa);
				
			}
		});
		
	}

	// -----------------------------
    // Crear el adaptador
    // -----------------------------
	private void setAdapter(){                                    
		
		//Creo una instancia del adaptador
		//Adaptador_Mensajes adaptador = new Adaptador_Mensajes(getConversacion(), this);
		//Adaptador_Mensajes adaptador = new Adaptador_Mensajes(getConversacion(), getApplicationContext());
		
		adaptador = new AdaptadorMoviles(listadoMoviles, ListaActivity.this);
		
		//Setea el adapatador a la lista
		listView.setAdapter(adaptador);
		
		listView.setVisibility(View.VISIBLE);
				                                                                                                                                                                                                                                                                                                                                  
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lista, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
        	Intent intento = new Intent(ListaActivity.this, AcercaDe.class);
        	startActivity(intento);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	

	 // ------------------------------------------
	 // clase para ejecutar una tarea asincronica de obtener moviles
	 // ------------------------------------------
	 public class obtenerMoviles extends AsyncTask<String , Void, Movil[]>
	 {   
		 ProgressDialog pd = new ProgressDialog(ListaActivity.this);
	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getResources().getString(R.string._dlg_ObteniendoMoviles));
			pd.show();
			pd.setCancelable(false);
			
			super.onPreExecute();
		}

		
		@Override
		protected Movil[] doInBackground(String... _texto) {
			
			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(ListaActivity.this);
			
			
			Movil[] _rta;
			_rta = cw.ObtenerMoviles("" + _texto[0]);
			
			
			return _rta;
		}
	
		
		@Override
		protected void onPostExecute(Movil[] resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();


			if ( resultado.length >= 1)   
			{
				listadoMoviles = new ArrayList<Movil>();
				for (Movil movil : resultado) {
					listadoMoviles.add(movil);
				}
			    //Setear adaptador
				setAdapter();
			}
			else
			{

		        Builder builder = new AlertDialog.Builder (ListaActivity.this);
		        builder.setTitle (getString(R.string.app_name))
		               .setMessage (getString(R.string._dlg_ErrorObteniendoMoviles))
		               .setNeutralButton(getString(R.string._ok),null);
	               
		        builder.create().show ();
				return;
			}
			
			super.onPostExecute(resultado);
		}
	  }
	 
}
