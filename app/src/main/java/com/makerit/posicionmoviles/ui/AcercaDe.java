package com.makerit.posicionmoviles.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.makerit.posicionmoviles.R;

public class AcercaDe extends Activity {

	LinearLayout mail;
    LinearLayout tel;
    LinearLayout mapa;
    LinearLayout web;

    ImageView iv_facebook;
    ImageView iv_yt;
    ImageView iv_ins;
    ImageView iv_lin;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_acerca_de);

        mail = (LinearLayout)findViewById(R.id.mail);
        tel = (LinearLayout)findViewById(R.id.tel);
        mapa = (LinearLayout)findViewById(R.id.mapa);
        web = (LinearLayout)findViewById(R.id.web);

        iv_facebook = (ImageView) findViewById(R.id.iv_facebook);
        iv_yt = (ImageView)findViewById(R.id.iv_yt);
        iv_ins = (ImageView)findViewById(R.id.iv_ins);
        iv_lin = (ImageView)findViewById(R.id.iv_lin);

        mail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL, "info@makerelectronica.com.ar");
                startActivity(Intent.createChooser(intent, "Enviar Email"));

            }
		});

        tel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String uri = "tel: + 54 9 223-475 6280";
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(uri));
                startActivity(intent);

            }
        });

        mapa.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=Chile 1816, Mar del Plata, Buenos Aires, Argentina");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

            }
        });

        web.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String url = "http://www.makerelectronica.com.ar";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });

        iv_facebook.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String url = "https://www.facebook.com/maker.ar/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });

        iv_yt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String url = "https://www.youtube.com/channel/UC5hlDSLcgpSbGZTVc4iiKNg";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });

        iv_lin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String url = "https://www.linkedin.com/company-beta/24799619/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });


        iv_ins.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String url = "https://www.instagram.com/maker.ar/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });


	}

}
