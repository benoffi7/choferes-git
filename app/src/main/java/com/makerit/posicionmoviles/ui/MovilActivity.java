package com.makerit.posicionmoviles.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.makerit.posicionmoviles.R;
import com.makerit.posicionmoviles.objetos.Movil;
import com.makerit.posicionmoviles.soap.conexionServicioWebSOAP;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;
import java.util.LinkedList;

public class MovilActivity extends AppCompatActivity  {


	MapView map ;
	IMapController myMapController;
	Spinner sp;
	TabHost tabs;
	LinearLayout layout_Mapa ;
	TextView tv_ubicacion, tv_fechahora, tv_velocidad, tv_tiempodetenido, tv_movil, textView;
	String _idMovil;

	
	@SuppressWarnings("rawtypes")
	LinkedList movilesSpinner;
	
    @SuppressWarnings("rawtypes")
	ArrayAdapter spinner_adapter;
    
	 @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_moviles);
		  
	  	if (getIntent() != null)
		{
			// Tiene extras ? 
			if (getIntent().getExtras() != null)
			{
				if (getIntent().getExtras().getString("idMovil") != null)
				{
					_idMovil = getIntent().getExtras().getString("idMovil");
				}
			}
		}
	  	
		findViews();
		
		setEvents();
		
		obtenerDatosMoviles odm = new obtenerDatosMoviles();
		odm.execute(_idMovil);
        
	}
	 
	 

	@SuppressWarnings("rawtypes")
	private void findViews() {
		
		tv_movil = (TextView)findViewById(R.id.pos_textView_movil);
		
    	movilesSpinner = new LinkedList();
        tabs=(TabHost)findViewById(android.R.id.tabhost);
        tv_ubicacion = (TextView)findViewById(R.id.tv_ubicacion);
        tv_fechahora = (TextView)findViewById(R.id.tv_fechahora);
        tv_velocidad = (TextView)findViewById(R.id.tv_velocidad);
        textView = (TextView)findViewById(R.id.textView);
        tv_tiempodetenido = (TextView)findViewById(R.id.tv_tiempodetenido);
        layout_Mapa = (LinearLayout) findViewById(R.id.linearLayout_Mapa);
        
        tabs.setup();
        TabHost.TabSpec spec=tabs.newTabSpec("mitab1");
        spec.setContent(R.id.linearLayout_Mapa);
        spec.setIndicator("", getResources().getDrawable(R.drawable.mundo) );
        tabs.addTab(spec);
        spec=tabs.newTabSpec("mitab2");
        spec.setContent(R.id.tab2);
        spec.setIndicator("", getResources().getDrawable(R.drawable.lista) );
        tabs.addTab(spec);
        tabs.setCurrentTab(0);
	
        map = (MapView) findViewById(R.id.mapView);
        map.setTileSource(TileSourceFactory.MAPNIK);
        
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        map.setClickable(true);
        map.setAlwaysDrawnWithCacheEnabled(true);
 
        // Asignar el MapView como Layout de la Activity
	    //layout_Mapa.addView(mapView);
	    tabs.setVisibility(View.INVISIBLE);
	    
	}

	@Override
	public boolean onSupportNavigateUp(){
	    finish();
	    return true;
	}
	
    private void setEvents()
    {
        findViewById(R.id.imageView2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                startActivity(new Intent(MovilActivity.this,AcercaDe.class));
            }
        });
        tabs.getTabWidget().getChildAt(0).setBackgroundResource(R.color.celeste_seleccion);
        tabs.getTabWidget().getChildAt(1).setBackgroundResource(R.color.fondo_edit);

        tabs.setOnTabChangedListener(new TabHost.OnTabChangeListener()
        {
            public void onTabChanged(String arg0)
            {
                for (int i = 0; i < tabs.getTabWidget().getChildCount(); i++)
                {
                    tabs.getTabWidget().getChildAt(i).setBackgroundResource(R.color.celeste_seleccion); // unselected
                }
                tabs.getTabWidget().getChildAt(tabs.getCurrentTab()).setBackgroundResource(R.color.fondo_edit); // selected
            }
        });
    }
    
    // --------------------------------------------
    // --------------------------------------------
	private void MostrarMapa(final Movil _movil) {

		// centrar y poner zoom
		 IMapController mapController = map.getController();
	     mapController.setZoom(16);
		
        // create a GeoPoint with the latitude and longitude coordinates
        GeoPoint geoPoint = new GeoPoint(_movil.getLatitud(), _movil.getLongitud()); //-37.99900,	-57.54794

        // create an ArrayLista para poner el overlay que tiene el marker
        ArrayList<OverlayItem> arregloDeOverlays;
        arregloDeOverlays = new ArrayList<OverlayItem>();
         
        arregloDeOverlays.add(new OverlayItem("", "", geoPoint));
        
        //marker perzonalizado
        Drawable marker = toBitmapDrawable(getResources().getDrawable(R.drawable.ic_action_location_blue));
        
        // Si hay overlays, borrarlos
        if (!map.getOverlays().isEmpty())
        	map.getOverlays().clear();
  
        ItemizedIconOverlay<OverlayItem> overlayAAgregar = new ItemizedIconOverlay<>(arregloDeOverlays, marker, ListenerClickMarker, new DefaultResourceProxyImpl(getApplicationContext()));
        
     //   ItemizedOverlayWithFocus<OverlayItem> overlayAAgregar2 = new ItemizedOverlayWithFocus<OverlayItem>(this, arregloDeOverlays, ListenerClickMarker);
        map.getOverlays().add(overlayAAgregar);

        mapController.setCenter(new IGeoPoint() {
			
			@Override
			public int getLongitudeE6() {

				return 0;
			}
			
			@Override
			public double getLongitude() {
				return (double)_movil.getLongitud();
			}
			
			@Override
			public int getLatitudeE6() {
				return 0;
			}
			
			@Override
			public double getLatitude() {

				return _movil.getLatitud();
			}
		});
        
        tv_ubicacion.setText(_movil.getUbicacion());
        tv_fechahora.setText(_movil.getUltEncuesta());
        tv_velocidad.setText(_movil.getVelocidad() + " Km/h");
        tv_tiempodetenido.setText(_movil.getTiempoDetenido() + " HH:mm");
        tv_movil.setText(getResources().getString(R.string._tar_movil) + ": " + _movil.getDescripcion().split("-")[0]);
        textView.setText("Dominio" + ": " + _movil.getDescripcion().split("-")[1]);

        
//        MinimapOverlay miniMapOverlay = new MinimapOverlay(this, map.getTileRequestCompleteHandler());
//        miniMapOverlay.setZoomDifference(8);
//        miniMapOverlay.setHeight(100);
//        miniMapOverlay.setWidth(100);
//        map.getOverlays().add(miniMapOverlay);
//        
        tabs.setVisibility(View.VISIBLE);
	}
    
	OnItemGestureListener<OverlayItem> ListenerClickMarker = new OnItemGestureListener<OverlayItem>() {
   	 
        @Override
        public boolean onItemLongPress(int arg0, OverlayItem arg1) {
            // TODO Auto-generated method stub
            return false;
        }
     
        @Override
        public boolean onItemSingleTapUp(int index, OverlayItem item) {
            Toast.makeText(
            	MovilActivity.this,
            	tv_ubicacion.getText(),
                Toast.LENGTH_LONG).show();
                 
            return true;
        }
     
    };
    
    // Para convertir un bitmapa para que sea un marker
    private Drawable toBitmapDrawable(Drawable anyDrawable){ 
        Bitmap bmp = Bitmap.createBitmap(anyDrawable.getIntrinsicWidth(), anyDrawable.getIntrinsicHeight(), Config.ARGB_8888); 
        Canvas canvas = new Canvas(bmp);
        anyDrawable.setBounds(0, 0, anyDrawable.getIntrinsicWidth(), anyDrawable.getIntrinsicHeight());
        anyDrawable.draw(canvas);
        return new BitmapDrawable(getResources(), bmp);
    }
    
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.moviles, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
        	Intent intento = new Intent(MovilActivity.this, AcercaDe.class);
        	startActivity(intento);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	

	
	
	
	   // ------------------------------------------
    // ------------------------------------------
    public class obtenerDatosMoviles extends AsyncTask<String , Void, Movil>
    
	{   
    	ProgressDialog pd = new ProgressDialog(MovilActivity.this);
    	
		// Se ejecuta antes de llamar a la tarea
		@Override 
		protected void onPreExecute() {
			pd.setMessage(getResources().getString(R.string._dlg_ObteniendoDatosMovil));
			pd.show();
			pd.setCancelable(false);
			
			super.onPreExecute();
		}

		
		@Override
		protected Movil doInBackground(String... _texto) {
			
			conexionServicioWebSOAP cw = new conexionServicioWebSOAP(MovilActivity.this);
			
			
			Movil _rta;
			_rta = cw.ObtenerDatosDeMovil("" + _texto[0]);
			
			
			return _rta;
		}
	
		
		@Override
		protected void onPostExecute(Movil resultado) {
			
			if (pd.isShowing() || pd != null)
				pd.dismiss();


			if ( resultado.getLatitud() != 0)   
			{
				MostrarMapa(resultado);
			}
			else
			{

		        Builder builder = new AlertDialog.Builder (MovilActivity.this);
		        builder.setTitle (getString(R.string.app_name))
		               .setMessage (getString(R.string._dlg_ErrorObteniendoDatosMovil))
		               .setNeutralButton(getString(R.string._ok),null);
	               
		        builder.setOnDismissListener(new OnDismissListener() {
					
					@Override
					public void onDismiss(DialogInterface dialog) {
						finish();
						
					}
				});
		        builder.create().show ();
		        
		        

			}
			
			super.onPostExecute(resultado);
		}
	  }

    
}
