package com.makerit.posicionmoviles.adap;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makerit.posicionmoviles.R;
import com.makerit.posicionmoviles.objetos.Movil;
import com.makerit.posicionmoviles.ui.MovilActivity;

import java.util.ArrayList;


public class AdaptadorMoviles extends BaseAdapter {

	// Array con los mensajes
	ArrayList<Movil> moviles;
	
	// Direccion, es el objeto que uso cuando vuelve del adaptador para tomar los datos en Activity_favoritas
	Movil movil = new Movil();
	
	// Obtener el contexto
	Context contexto;
	
	// id de seleccionada para que se pinten bien
	private String _idSeleccion = "-5";
	
	// Generar constructor con Source -> Generate constructor
	public AdaptadorMoviles(ArrayList<Movil> moviles, Context Contexto) {
		super();
		// Se usa el this porque las dos variables se llaman iguales
		// el this
		this.moviles = moviles;
		contexto = Contexto;
	}

	
	// Lo siguiente se genera solo con add unimplements methods
	
	// Obtener la cantidad de items
	@Override
	public int getCount() {
		
		return moviles.size();
	}

	// Obtener el item por posicion
	@Override
	public Movil getItem(int _pos) {
		return moviles.get(_pos);
	}

	// no se usa
	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int _pos, View _vistaFila, final ViewGroup _vistaGrupo) {

		
		final Movil oMovil = getItem(_pos);
		
		//defino un contedor
		final viewHolder _vistaFila_contenedora;
		
		if (_vistaFila == null){
			
			//Instanciar el servicio de inflador en su contexto
			LayoutInflater inflador = (LayoutInflater)contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			//transformo un xml en una variable Java 
			_vistaFila = inflador.inflate(R.layout.tarjeta_moviles, _vistaGrupo, false);
			
			//creamos una instancia de fila_Aux
			_vistaFila_contenedora = new viewHolder();
			
			//asignamos los valores, es como una activity, para poner los valores de la clase MENSAJE
			_vistaFila_contenedora.nroMovil = (TextView)_vistaFila.findViewById(R.id.ad_NroMovil);
			_vistaFila_contenedora.hora = (TextView)_vistaFila.findViewById(R.id.ad_Hora);
			_vistaFila_contenedora.dominio = (TextView)_vistaFila.findViewById(R.id.ad_Dominio);
			_vistaFila_contenedora.ubicacion = (TextView)_vistaFila.findViewById(R.id.ad_Ubicacion);
			_vistaFila_contenedora.Imagen = (ImageView)_vistaFila.findViewById(R.id.imageAutito);
		//	_vistaFila_contenedora.Imagen.setVisibility(View.INVISIBLE);

			// BUscar el RelativeLayout para que me sirva para  el click
			// el del xml activuti_renglon, es el click sobre el activity que en realidad lo veo com olinea
			_vistaFila_contenedora.RelativeLayout_Fila = (LinearLayout)_vistaFila.findViewById(R.id.ad_linealPrincipal);
			
			// PAra el formato y colores
			//_vistaFila_contenedora.listView_direcciones = (LinearLayout) _vistaFila.findViewById(R.id.ad_linealDatos);
			
			// Imagen
			//_vistaFila_contenedora.Imagen = (ImageView)_vistaFila.findViewById(R.id.image);

			
			_vistaFila.setTag(_vistaFila_contenedora);
		}
		else
        {
			//levanto los metadatos de la fila para acceder a sus componentes ya instanciados la 1ra vez
			_vistaFila_contenedora = (viewHolder)_vistaFila.getTag();	


		}
		
		// setear
		_vistaFila_contenedora.objMovilSeleccionado = oMovil;
		
		_vistaFila_contenedora.nroMovil.setText(oMovil.getNroMovil());
		_vistaFila_contenedora.dominio.setText(oMovil.getDominio());
		_vistaFila_contenedora.ubicacion.setText(oMovil.getUbicacion());
		_vistaFila_contenedora.hora.setText(oMovil.getUltEncuesta());
		_vistaFila_contenedora.Imagen.setImageDrawable(Seleccionar_imagen(oMovil));
		_vistaFila_contenedora.Imagen.setTag("imagen");

		
//		_vistaFila_contenedora.imagen.setBackgroundColor(Color.rgb(oMensaje.is_salida()? 255: 210, 175, 12));
		
		
		//siempre se trabaja con el holder porque es el que tiene los componentes XML
		//Se pueden asignar eventos y propiedades
		 // Click
		_vistaFila_contenedora.RelativeLayout_Fila.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				movil = _vistaFila_contenedora.objMovilSeleccionado;

				
				for (int i = 0; i < _vistaGrupo.getChildCount(); i++) {
					//_vistaGrupo.getChildAt(i).setBackgroundResource(R.drawable.plana);
					//((ImageView)_vistaGrupo.getChildAt(i).findViewById(R.id.imageAutito)).setVisibility(View.INVISIBLE);
				}
				
				
				//_vistaFila_contenedora.RelativeLayout_Fila.setBackgroundColor(contexto.getResources().getColor(R.color.color_agencia));
				//_vistaFila_contenedora.RelativeLayout_Fila.setBackgroundResource(R.drawable.tarjeta);
				_vistaFila_contenedora.Imagen.setVisibility(View.VISIBLE);
				

			}
			
		});
		
		_vistaFila_contenedora.Imagen.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				for (int i = 0; i < _vistaGrupo.getChildCount(); i++) {
					//_vistaGrupo.getChildAt(i).setBackgroundResource(R.drawable.plana);
					
				}

				//_vistaFila_contenedora.RelativeLayout_Fila.setBackgroundColor(contexto.getResources().getColor(R.color.color_agencia));
				//vistaFila_contenedora.RelativeLayout_Fila.setBackgroundResource(R.drawable.tarjeta);
				
				
				Intent intento = new Intent(contexto, MovilActivity.class);
				intento.putExtra("idMovil", oMovil.getId());
 				contexto.startActivity(intento);

			}
		});
		
		

		
		return _vistaFila;
		
	}

	
	private Drawable Seleccionar_imagen(Movil oMovil) {
		Drawable _rta = contexto.getResources().getDrawable(R.drawable.no_transmite);
		
		if (oMovil.getTransmitiendo().equals("1"))
		{
			if (oMovil.getVelocidad().equals("0"))
				  _rta = contexto.getResources().getDrawable(R.drawable.detenido);
			else
				_rta = contexto.getResources().getDrawable(R.drawable.movimiento);
		}
		
		return _rta;
	}


	// -----------------------------------
	// tiene que tener tantas variables como campos tenga el XML o clase
	// -----------------------------------
	class viewHolder {
		TextView nroMovil;
		TextView dominio;
		TextView hora;
		TextView ubicacion;
		Movil objMovilSeleccionado;

        LinearLayout RelativeLayout_Fila;
		//LinearLayout listView_direcciones;
		ImageView Imagen;
	}	
	
}
